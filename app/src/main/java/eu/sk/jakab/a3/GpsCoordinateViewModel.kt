package eu.sk.jakab.a3

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.room.Room


/**
 * Created by jakab on 12/10/2021.
 */
class GpsCoordinateViewModel(application: Application) : AndroidViewModel(application) {
    private val locationData = GpsCoordinateLiveData(application)

    fun getLastGpsCoordinate(): LiveData<GpsCoordinate> {
        return locationData
    }
}