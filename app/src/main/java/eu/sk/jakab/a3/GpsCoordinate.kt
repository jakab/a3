package eu.sk.jakab.a3

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by jakab on 12/10/2021.
 */

@Entity
data class GpsCoordinate
    (
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val lat: Double,
    val lon: Double
)