package eu.sk.jakab.a3

import androidx.lifecycle.LiveData
import androidx.room.*


/**
 * Created by jakab on 12/10/2021.
 */
@Dao
interface GpsDAO {
    @Insert
    fun Insert(gpsCoordinate: GpsCoordinate)
}