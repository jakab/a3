package eu.sk.jakab.a3

import androidx.room.Database
import androidx.room.RoomDatabase


/**
 * Created by jakab on 12/10/2021.
 */
@Database(entities = [GpsCoordinate::class], version = 1, exportSchema = false)
abstract class GpsDatabase : RoomDatabase() {
    abstract fun gpsDao(): GpsDAO
}